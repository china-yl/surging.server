FROM microsoft/dotnet:2.2-aspnetcore-runtime
ENV Surging_Server_Port 98
COPY . /publish
WORKDIR /publish
EXPOSE 98
ENTRYPOINT ["dotnet","Surging.Server.dll"]